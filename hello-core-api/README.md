# Setup

1. Install bower globally ```npm install bower -g```
2. Run ```npm install```
3. Run ```bower install```
4. Edit package.json and change to your instance URL on line 6.
5. Run ```npm run server```
